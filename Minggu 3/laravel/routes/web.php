<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/','HomeController@home');
Route::get('/register','AuthController@regis');
Route::post('/welcome','AuthController@welcome');
route::get('/master',function(){
    return view('layout.master');
});
route::get('/data-table',function(){
    return view('page.data-table');
});